const crypto = require('crypto')

const getRoomById = (map, id) => map.get(id)
const isHex = value => /^#[0-9A-F]{6}$/i.test(value)
const getRandomString = () => crypto.randomBytes(8).toString('hex')
const isInRange = (x, begin, end) => (Number.isInteger(x) && (x > begin) && (x < end))
const getClients = (io, id) => {
    if (io.sockets.adapter.rooms[id] == undefined)
        return -1

    return io.sockets.adapter.rooms[id].length
}

const rooms = new Map()

module.exports = class RoomController {
    constructor (io, socket) {
        this.io = io
        this.socket = socket
        this.update()
    }

    update () {
        this.io.emit('res.update', Array.from(rooms.values()))
    }

    create (room) {
        room.maxClients = parseInt(room.maxClients)
        room.width = parseInt(room.width)
        room.height = parseInt(room.height)

        const _maxClients = isInRange(room.maxClients, 0, 9) ? room.maxClients : 4
        const _width = isInRange(room.width, 0, 257) ? room.width * 16 : 1024
        const _height = isInRange(room.height, 0, 257) ? room.height * 16 : 1024
        
        const data = {
            name: room.name,
            id: getRandomString(),
            pos: [],
            maxClients: _maxClients,
            width: _width,
            height: _height,
            clients: 0,
        }
      
        rooms.set(data.id, data)
        this.join(data.id)
        this.socket.emit('res.created', data.id)
        this.update()
    }
    
    join (id) { // room consists of name, id, pos, maxClients, clients
        const roomToJoin = getRoomById(rooms, id)

        if (roomToJoin) {
            this.socket.join(id)
            const clients = getClients(this.io, id)

            if (clients > roomToJoin.maxClients) {
                this.leave(id)
                this.socket.emit('res.error', "Full")
            }
            else {
                roomToJoin.clients = clients
            
                this.socket.emit('res.join', roomToJoin)

                this.update()

                console.log(rooms)
            }
        }
        else
            this.socket.emit('res.leave')
    }
    
    leave (id) {
        const roomToLeave = getRoomById(rooms, id)
    
        if (roomToLeave) {
            this.socket.leave(id)
            roomToLeave.clients = getClients(this.io, id)
        
            this.socket.emit('res.leave')

            if (roomToLeave.clients < 1)
                this.remove(id)

            this.update()
        }
    }
    
    remove (id) {
        rooms.delete(id)
    }

    draw (data) {
        const roomToDraw = getRoomById(rooms, data.id)

        if (roomToDraw) {
            const exists = roomToDraw.pos.findIndex(obj => {
                return obj.dx === data.dx && obj.dy === data.dy
            })
            
            if(exists) roomToDraw.pos.splice(exists - 1, exists)

            if (data.color !== null) {
                if(!isHex(data.color)) data.color = '#000000'
            
                roomToDraw.pos.push(data)
            }

            this.io.in(data.id).emit('res.draw', data)
        }
    }

    clear (id) {
        console.log(id)
        const roomToClear = getRoomById(rooms, id)

        console.log(roomToClear)

        if (roomToClear) {
            roomToClear.pos = []
            this.io.in(id).emit('res.clear')
        }
    }
}