'use strict'

const express = require('express')
const compression = require('compression')
const RoomController = require('./room-controller.js')
const app = express()
const server = require('http').Server(app)
const io = require('socket.io')(server)

const PORT = process.env.PORT || 8000
app.set('port', PORT)

app.use(compression())
app.use(express.static('app'))

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/app/index.html')
})

server.listen(app.get('port'), () => console.log(`Listening on port ${app.get('port')}`))



io.on('connection', socket => {
    const session = new RoomController(io, socket)

    socket.on('req.update', () => {
        session.update()
    })
    
    socket.on('req.create', roomInfo => {
        session.create(roomInfo)
    })

    socket.on('req.join', room => {
        session.join(room)
    })

    socket.on('req.leave', room => {
        session.leave(room)
    })

    socket.on('req.draw', data => {
        session.draw(data)
    })
    
    socket.on('req.clear', id => {
        session.clear(id)
    })
})

app.get('/', (req, res) => {
})
